function! EditClip_load() abort
  normal "+p
endfunction

function! EditClip_drop_empty_line() abort
  silent 1g/^$/d
  set nomodified
endfunction

function! EditClip_overwrite() abort
  normal ggvG$h"+yg;
  set nomodified
endfunction

augroup editclip
  autocmd!
  autocmd BufReadCmd editclip://* call EditClip_load()
  autocmd BufWinEnter editclip://* call EditClip_drop_empty_line()
  autocmd BufWriteCmd editclip://* call EditClip_overwrite()
augroup END
