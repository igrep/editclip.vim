# editclip.vim

Instantly edit the clipboard in Vim/Neovim.

## Usage

After adding this repository to your `&runtimepath`, launch vim with this command:

```
vim editclip://clipboard
```

Now the content of the clipboard is loaded onto the buffer.  
Then save the buffer by `:w` (or any other command you like) to overwrite the clipboard with the buffer.
